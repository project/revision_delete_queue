# Revision Delete Queue

This module creates a queue of old node revisions to delete plus it defines a
queue worker to delete them. It is heavily inspired on Node Revision Delete
but splits in two the tasks of finding candidates and deleting them, which
speeds up the initial effort in deleting a large set of old revisions.

## Queue creation

1. Decide the minimum revision age that you want to keep. For example, if you want
to delete all revisions older than 12 months ago, you would run this command
to build the queue:

```
drush revision-delete-queue "12 months ago"
```

If you want to filter by a content type, here is an example:

```
drush revision-delete-queue --type=article "12 months ago"
```

The above command will take a few seconds to run the query to find the candidates,
create the queue, and then push the items to a queue so they can be processed
later.

## Queue processing

Cron won't run this queue since it has been set to be skipped at
`revision_delete_queue_cron_queue_info()`. Therefore it is up to you to define
how should it be processed.

Processing the queue can be done by Drush's `queue-run` command. Here is the command
to run the queue for 60 seconds:

```
drush queue-run revision_delete --time-limit=60
```

A basic setup would involve setting up a cron that runs the above command. Depending
on how fast you want to clear revisions and how busy is your site, you should adjust
both the time that the command runs and how often it runs.

## Advanced usage for databases with a large amount of revisions

It is not recommended to run the queue until it completes when there is a lot
of items to process since Drush could run out of memory.

In order to keep the queue processing as efficiently as possible, you could
set up a [supervisor](http://supervisord.org/) job that runs several
"drush queue-run" commands in parallel for short periods of time.

For example, the following configuration file which should be placed at
/etc/supervisor/conf.d/queue.conf defines a job that spawns three drush queue-run
commands that will process the queue in parallel. As soon as these three comands
complete, supervisor will spawn another three again.

```
juampy@localhost:~/nbcnews/Publisher7_nbcnews$ cat /etc/supervisor/conf.d/queue.conf 
[program:mysitequeue]
directory=/var/www/html/mysite
command=make drush " queue-run revision_delete --time-limit=54000"
autorestart=true
user=juampy
autorestart=true
stderr_logfile=/var/log/mysite.err.log
stdout_logfile=/var/log/mysite.out.log
redirect_stderr=true
environment=HOME="/home/juampy",USER="juampy"
process_name=%(program_name)s_%(process_num)02d
numprocs=3
```
