<?php

/**
 * @file
 * Drush commands related to the Revision Delete Queue module.
 */

/**
 * Implements hook_drush_command().
 */
function revision_delete_queue_drush_command() {
  $items['revision-delete-queue'] = array(
    'description' => dt('Finds candidate revisions and pushes them to a queue for background processing.'),
    'arguments' => array(
      'age' => dt('Minimum age of a revision to be deleted.'),
    ),
    'options' => array(
      'type' => dt('Content type machine name.'),
    ),
    'examples' => array(
      'drush rdq --type=article "12 months ago"' => dt('Deletes article revisions older than 12 months ago.'),
      'drush rdq "1 week ago"' => dt('Deletes all revisions older than one week.'),
    ),
    'aliases' => array('rdq'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  return $items;
}

/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_revision_delete_queue($age) {
  // Get candidates based on node type and revision age. This determines which
  // nodes have more than one revision based on the variables. Also limit
  // candidates by the minimum age variable.
  $type_condition = '';
  $query_arguments = array(':minimum_age_time' => strtotime($age));

  if (drush_get_option('type')) {
   $type_condition = ' AND n.type = :content_type';
   $query_arguments[':content_type'] = drush_get_option('type');
  }

  $result = db_query('SELECT nr.nid, nr.vid
    FROM {node_revision} nr
    INNER JOIN {node} n ON n.nid = nr.nid and n.vid <> nr.vid
    WHERE nr.timestamp < :minimum_age_time
    ' . $type_condition . '
    ORDER BY nid ASC', $query_arguments
  );

  $candidates = array();
  foreach ($result as $row) {
    $candidates[] = (array) $row;
  }

  $queue = DrupalQueue::get('revision_delete');
  $queue->createQueue();
  foreach ($candidates as $candidate) {
    $queue->createItem($candidate);
  }
}
